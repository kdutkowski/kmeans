#undef _GLIBCXX_ATOMIC_BUILTINS
#undef _GLIBCXX_USE_INT128

// includes, system
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>

#ifdef _WIN32
# define WINDOWS_LEAN_AND_MEAN
# define NOMINMAX
# include <windows.h>
#endif

#define _USE_MATH_DEFINES

// OpenGL Graphics includes
#include <GL/glew.h>
#if defined (__APPLE__) || defined(MACOSX)
#include <GLUT/glut.h>
#else
#include <GL/freeglut.h>
#endif

// includes, cuda
#include <cuda_runtime.h>
#include <cuda_gl_interop.h>

// Utilities and timing functions
#include <helper_functions.h> // includes cuda.h and cuda_runtime_api.h
#include <timer.h> // timing functions

// CUDA helper functions
#include <helper_cuda.h> // helper functions for CUDA error check
#include <helper_cuda_gl.h> // helper functions for CUDA/GL interop

#include <vector_types.h>

#include <thrust/device_ptr.h>
#include <thrust/sort.h>
#include <thrust/gather.h>
#include <thrust/iterator/counting_iterator.h>

#define MAX_EPSILON_ERROR 10.0f
#define THRESHOLD 0.30f
#define REFRESH_DELAY 10 //ms

#define DBG			(fprintf(stderr,"%s:%d:\t",__FILE__,__LINE__));

////////////////////////////////////////////////////////////////////////////////
// constants
#define MAX_DISTANCE 0.005
#define MIN_DISTANCE 0.001
#define MAX_ANGLE M_PI
#define MAX_SPEED 0.09
#define W_NEIGHBOUR_SPEED 0.15
#define W_NEIGHBOUR_DISTANCE 0.15
#define W_MIN_DISTANCE 0.15
#define W_NOISE 0.1
#define COMPARE(x, y) (((x) > (y)) - ((x) < (y)))
#define SIGN(x) COMPARE(x, 0)

#define MESH_SIZE (width*height)

#define NUM_COORDS 3

const unsigned int window_width = 800;
const unsigned int window_height = 800;

const unsigned int width = 100;
const unsigned int height = 100;

const unsigned int numClusters = 10;


// vbo variables
GLuint vbo;
struct cudaGraphicsResource *cuda_vbo_resource;
void *d_vbo_buffer = NULL;

//Table containing velocity of all particles. It is pointer to device memory
float2 *speed = NULL;
float3 *dptr = NULL;

// mouse controls
int mouse_old_x, mouse_old_y;
int mouse_buttons = 0;
float rotate_x = -3.0, rotate_y = 3.0;
float translate_z = -3.0;
bool isPaused = false;

// kmeans variables
int* membership = NULL;
float** clusterColors = NULL;
int *newClusterSize;     
float **clusters; 
float **newClusters;
float** obj = NULL;

StopWatchInterface *timer = NULL;

// Auto-Verification Code
int fpsCount = 0; // FPS count for averaging
int fpsLimit = 1; // FPS limit for sampling
int g_Index = 0;
float avgFPS = 0.0f;
unsigned int frameCount = 0;
unsigned int g_TotalErrors = 0;
bool g_bQAReadback = false;

int *pArgc = NULL;
char **pArgv = NULL;

////////////////////////////////////////////////////////////////////////////////
// declaration, forward
bool runTest(int argc, char **argv, char *ref_file);
void cleanup();

// GL functionality
bool initGL(int *argc, char **argv);

// rendering callbacks
void display();
void keyboard(unsigned char key, int x, int y);
void mouse(int button, int state, int x, int y);
void motion(int x, int y);
void timerEvent(int value);

// Cuda functionality
void runCuda(struct cudaGraphicsResource **vbo_resource);
void prepareCuda(struct cudaGraphicsResource **vbo_resource);
void checkResultCuda(int argc, char **argv, const GLuint &vbo);

/*-----------------------------------------------------------------------------------------------------*/

__inline static
float euclid_dist_2(int numdims, float *coord1, float *coord2)
{
    int i;
    float ans=0.0;

    for (i=0; i<numdims; i++)
        ans += (coord1[i]-coord2[i]) * (coord1[i]-coord2[i]);

    return(ans);
}

/*----< find_nearest_cluster() >---------------------------------------------*/
__inline static
int find_nearest_cluster(int numClusters, float *object, float **clusters) 
{
    int index, i;
    float dist, min_dist;

    index = 0;
	min_dist = euclid_dist_2(NUM_COORDS, object, clusters[0]);

    for (i=1; i<numClusters; i++) {
        dist = euclid_dist_2(NUM_COORDS, object, clusters[i]);
        if (dist < min_dist) { 
            min_dist = dist;
            index = i;
        }
    }
    return(index);
}

float** CPUkmeans(float **objects,  
                   int numObjs,
                   int numClusters,
                   float threshold,
                   int *membership, 
                   int *loop_iterations)
{
    int i, j, index, loop=0;
	float delta; 

    for (i=1; i<numClusters; i++)
        clusters[i] = clusters[i-1] + NUM_COORDS;

    for (i=0; i<numClusters; i++)
        for (j=0; j<NUM_COORDS; j++)
            clusters[i][j] = objects[i][j];

    for (i=0; i<numObjs; i++) membership[i] = -1;

   
    for (i=1; i<numClusters; i++)
        newClusters[i] = newClusters[i-1] + NUM_COORDS;

    do {
        delta = 0.0;
        for (i=0; i<numObjs; i++) {

            index = find_nearest_cluster(numClusters, objects[i], clusters);

            if (membership[i] != index) delta += 1.0;

            membership[i] = index;

            newClusterSize[index]++;
            for (j=0; j<NUM_COORDS; j++)
                newClusters[index][j] += objects[i][j];
        }

        for (i=0; i<numClusters; i++) {
            for (j=0; j<NUM_COORDS; j++) {
                if (newClusterSize[i] > 0)
                    clusters[i][j] = newClusters[i][j] / newClusterSize[i];
                newClusters[i][j] = 0.0;
            }
            newClusterSize[i] = 0;
        }

        delta /= numObjs;
    } while (delta > threshold && loop++ < 500);

    *loop_iterations = loop + 1;

    return clusters;
}

/*-----------------------------------------------------------------------------------------------------*/


inline __host__ __device__ float4 operator+(const float4 &a, const float4 &b)
{
	return make_float4(a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w);
}
inline __host__ __device__ float2 operator+(const float2 &a, const float2 &b)
{
	return make_float2(a.x + b.x, a.y + b.y);
}
inline __host__ __device__ float4 operator-(const float4 &a, const float4 &b)
{
	return make_float4(a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w);
}
inline __host__ __device__ float dot(float4 a, float4 b)
{
	return a.x*b.x + a.y*b.y + a.z*b.z + a.w*b.w;
}
inline __host__ __device__ float distance(float4 pt1, float4 pt2)
{
	float4 v = pt2 - pt1;
	return sqrt(dot(v,v));
}
inline __host__ __device__ float FastArcTan(float x)
{
	return M_PI_4*x - x*(fabs(x) - 1)*(0.2447 + 0.0663*fabs(x));
}
inline __device__ __host__ float random(float seed)
{
	int x = 88675123;
	int y = 362436069;
	int z = 521288629;

	x = (y *((int) seed) + z) % x;

	return sinf(x);
}
struct equalOperator {
	__host__ __device__
	bool operator()(const float4 x, const float4 y) const {
		return ( x.x > y.x );
	}
};


void chooseClusterColors()
{
	srand(1311);
	for (int i = 0; i < numClusters; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			float random = (float)(rand() / ((float)RAND_MAX)); 
			clusterColors[i][j] = random; 
		}
	}
}

int main(int argc, char **argv)
{
	char *ref_file = NULL;

	pArgc = &argc;
	pArgv = argv;

	speed = new float2[MESH_SIZE];
	dptr = new float3[MESH_SIZE];

	clusters = (float**) malloc(numClusters * sizeof(float*));
    assert(clusters != NULL);
    clusters[0] = (float*) malloc(numClusters * NUM_COORDS * sizeof(float));
    assert(clusters[0] != NULL);
	
	newClusterSize = (int*) calloc(numClusters, sizeof(int));
    assert(newClusterSize != NULL);

    newClusters = (float**) malloc(numClusters * sizeof(float*));
    assert(newClusters != NULL);
    newClusters[0] = (float*) calloc(numClusters * NUM_COORDS, sizeof(float));
    assert(newClusters[0] != NULL);

	clusterColors = (float**) malloc(numClusters * sizeof(float*));
	assert(clusterColors != NULL);
	
	for (int i = 0; i < numClusters; i++)
	{
		clusterColors[i] = (float*) malloc (3 * sizeof(float));
		assert(clusterColors[i] != NULL);
	}

	for (int i = 0; i < numClusters; i++)
		for (int j = 0; j < 3; j++)
			clusterColors[i][j] = .5f;

	chooseClusterColors();

	printf("starting...\n");

	runTest(argc, argv, ref_file);

	cudaDeviceReset();
	printf("completed, returned %s\n", (g_TotalErrors == 0) ? "OK" : "ERROR!");
	exit(g_TotalErrors == 0 ? EXIT_SUCCESS : EXIT_FAILURE);
}

void computeFPS()
{
	frameCount++;
	fpsCount++;

	if (fpsCount == fpsLimit) {
		avgFPS = 1.f / (sdkGetAverageTimerValue(&timer) / 1000.f);
		fpsCount = 0;
		fpsLimit = (int)MAX(avgFPS, 1.f);

		sdkResetTimer(&timer);
	}

	char fps[256];
	sprintf(fps, "CPU: %3.1f FPS \t(X:%d\t Y:%d)", avgFPS, mouse_old_x, mouse_old_y);
	glutSetWindowTitle(fps);
}

bool initGL(int *argc, char **argv)
{
	glutInit(argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
	glutInitWindowSize(window_width, window_height);
	glutCreateWindow("Cuda GL Interop (VBO)");
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMotionFunc(motion);
	glutTimerFunc(REFRESH_DELAY, timerEvent,0);

	// initialize necessary OpenGL extensions
	glewInit();

	if (! glewIsSupported("GL_VERSION_2_0 ")) {
		fprintf(stderr, "ERROR: Support for necessary OpenGL extensions missing.");
		fflush(stderr);
		return false;
	}

	// default initialization
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glDisable(GL_DEPTH_TEST);

	// viewport
	glViewport(0, 0, window_width, window_height);

	// projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0, (GLfloat)window_width / (GLfloat) window_height, 0.1, 10.0);

	SDK_CHECK_ERROR_GL();

	return true;
}



void prepare_positions(float3 *pos, float time)
{
	for (int index = 0;index<MESH_SIZE;index++)	
	{

		float u = (index / width) / (float) width;
		float v = (index % width) / (float) height;
		u = u*2-1;
		v = v*2-1;

		// calculate simple sine wave pattern
		float freq = 4.0f;
		float w = (u*u-v*v) * sinf(u+time);
		pos[index] = make_float3(u, v, w);
	}
}

void runCuda()
{
	//launch_kernel(dptr, width, height, speed);
	if (membership == NULL) {
		membership = new int[height*width];
	}

	static double t;
	t += 0.01;
	prepare_positions(dptr, t);


	if (obj == NULL) {
		obj = new float*[height*width];
		for (int i=0;i<height*width;i++) {
				obj[i] = (float*)(&dptr[i]);
		}
	}

	int loops;


	CPUkmeans(obj, MESH_SIZE, numClusters, 0.01, membership, &loops);
	printf("Loops: %d\n", loops);
}

bool runTest(int argc, char **argv, char *ref_file)
{
	//prepareCuda();
	// Create the CUTIL timer
	sdkCreateTimer(&timer);

	if (false == initGL(&argc, argv)) {
		return false;
	}

	// register callbacks
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);
	glutMotionFunc(motion);


	// start rendering mainloop
	glutMainLoop();
	atexit(cleanup);

	return true;
}

void setGLColor(int index)
{
	for (int i = 0; i < numClusters; i++)
	{
		if (membership[index] == i)
		{
			glColor3f( clusterColors[i][0], clusterColors[i][1], clusterColors[i][2]);
		}
	}
}

void display()
{
	sdkStartTimer(&timer);

	runCuda();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// set view matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0, 0.0, translate_z);
	glRotatef(rotate_x, 1.0, 0.0, 0.0);
	glRotatef(rotate_y, 0.0, 1.0, 0.0);

	glBegin( GL_POINTS );
	glPointSize(15);
	for ( int i = 0; i < width*height; ++i )
	{
		setGLColor(i);
		glVertex3f( dptr[i].x, dptr[i].y, dptr[i].z );
	}
	glEnd();
	glFinish();
	glutSwapBuffers();

	sdkStopTimer(&timer);
	computeFPS();
}

void timerEvent(int value)
{
	glutPostRedisplay();
	glutTimerFunc(REFRESH_DELAY, timerEvent,0);
}

void cleanup()
{
	for (int i = 0; i < numClusters; i++)
		free(clusterColors[i]);
	free(clusterColors);
	free(newClusters[0]);
    free(newClusters);
    free(newClusterSize);
	sdkDeleteTimer(&timer);
}


void keyboard(unsigned char key, int /*x*/, int /*y*/)
{
	switch (key) {
	case (27) :
		exit(EXIT_SUCCESS);
	break;
	}
}

void mouse(int button, int state, int x, int y)
{
	if (state == GLUT_DOWN) {
		mouse_buttons |= 1<<button;
	} else if (state == GLUT_UP) {
		mouse_buttons = 0;
	}

	mouse_old_x = x;
	mouse_old_y = y;
}

void motion(int x, int y)
{
    float dx, dy;
    dx = (float)(x - mouse_old_x);
    dy = (float)(y - mouse_old_y);

    if (mouse_buttons & 1)
    {
        rotate_x += dy * 0.2f;
        rotate_y += dx * 0.2f;
    }
    else if (mouse_buttons & 4)
    {
        translate_z += dy * 0.01f;
    }

    mouse_old_x = x;
    mouse_old_y = y;
}
